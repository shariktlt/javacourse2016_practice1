package ru.innopolis.sharypov.practice1.reporters;

import ru.innopolis.sharypov.practice1.RegistryHandler;

/**
 * Created by innopolis on 13.10.16.
 */
public interface ReporterInterface {
    void reportRegistry(RegistryHandler registryHandler);
}
