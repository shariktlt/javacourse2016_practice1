package ru.innopolis.sharypov.practice1.reporters;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import ru.innopolis.sharypov.practice1.RegistryHandler;
import ru.innopolis.sharypov.practice1.ResultEntry;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by innopolis on 13.10.16.
 */
public class FileReporter implements ReporterInterface {
    private FileOutputStream fileOutputStream;
    private String path;
    private Logger logger = LoggerFactory.getLogger(FileReporter.class);
    public FileReporter(String path) {
        this.path = path;
    }

    @Override
    public void reportRegistry(RegistryHandler registryHandler) {
        Set<Map.Entry<String, ResultEntry>> results = registryHandler.getResults();
        Iterator iterator = results.iterator();
        try{
            File file = new File(path);
            fileOutputStream = new FileOutputStream(file);
            if (!file.exists()) {
                file.createNewFile();
            }
            while(iterator.hasNext()) {
                Map.Entry<String, ResultEntry> entry = (Map.Entry<String, ResultEntry>) iterator.next();
                byte[] content = new String(entry.getKey() + ":" + entry.getValue().value()+"\n").getBytes();
                fileOutputStream.write(content);
            }
        }catch (Exception e){
            logger.error("FileReporter got exception", e);
        }finally {
            try{
                if(fileOutputStream != null){
                    fileOutputStream.flush();
                    fileOutputStream.close();
                }
            }catch (Exception e){

            }
        }
    }
}
