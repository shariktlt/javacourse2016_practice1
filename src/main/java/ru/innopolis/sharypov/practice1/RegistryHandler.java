package ru.innopolis.sharypov.practice1;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Created by innopolis on 12.10.16.
 */
public class RegistryHandler {
    private static Logger logger = LoggerFactory.getLogger(RegistryHandler.class);
    private volatile boolean isProcessing = true;
    private Map<String,ResultEntry> map = new ConcurrentHashMap<>();

    public boolean isProcessing() {
        return isProcessing;
    }

    public void setProcessing(boolean processing) {
        isProcessing = processing;
    }

    public void register(String key){
        map.putIfAbsent(key, new ResultEntry());
        ResultEntry entry = map.get(key);
        if(entry != null){
            entry.incriment();
            logger.debug("Register {}", key);
        }else{
            logger.error("Null ResultEntry @{}", key);
        }

    }

    public Set<Map.Entry<String, ResultEntry>> getResults(){
       return map.entrySet();
    }
}
