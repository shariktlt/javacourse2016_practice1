package ru.innopolis.sharypov.practice1.resource.loaders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.sharypov.practice1.resource.ResourceObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by innopolis on 11.10.16.
 */
public class HttpResource implements LoadableResource {
    private Logger logger = LoggerFactory.getLogger(HttpResource.class);

    @Override
    public String loadString(ResourceObject resourceObject) {
        URL url;
        URLConnection connection;
        String res = null;
        InputStream is = null;
        BufferedReader rd = null;
        try {
            SSLTool.disableCertificateValidation();
            url = new URL(resourceObject.getRaw());
            connection = url.openConnection();
            if(is == null) {
                is = connection.getInputStream();
            }
            if(rd == null) {
                rd = new BufferedReader(new InputStreamReader(is));
            }
            StringBuffer response = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            res = response.toString();

        }catch(Exception e){
            logger.error("HttpResource got exception", e);
        }finally {
            if(is != null){
                try {
                    is.close();
                } catch (IOException e) {
                    logger.error("Close exception", e);
                }
            }
            if(rd != null){
                try {
                    rd.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return res;
    }

    @Override
    public Object getNewObject() {
        return new HttpResource();
    }
}
