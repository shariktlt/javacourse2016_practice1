package ru.innopolis.sharypov.practice1.resource;

/**
 * Created by shariktlt on 15.10.16.
 */
public interface FactoryInterface {
    Object getNewObject();
}
