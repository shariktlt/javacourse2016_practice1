package ru.innopolis.sharypov.practice1.resource.validators;

import ru.innopolis.sharypov.practice1.resource.FactoryInterface;

/**
 * Created by innopolis on 12.10.16.
 */
public interface ResourceValidator extends FactoryInterface {
    public boolean isValid(String string);
}
