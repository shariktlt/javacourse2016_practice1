package ru.innopolis.sharypov.practice1.resource.processors;

import ru.innopolis.sharypov.practice1.RegistryHandler;
import ru.innopolis.sharypov.practice1.resource.FactoryInterface;
import ru.innopolis.sharypov.practice1.resource.ResourceObject;
import ru.innopolis.sharypov.practice1.resource.validators.ResourceValidator;

/**
 * Created by innopolis on 12.10.16.
 */
public interface ResourceProcessor extends FactoryInterface {
    void processResource(ResourceObject resourceObject, RegistryHandler registryHandler, ResourceValidator resourceValidator);
    void setResourceValidator(ResourceValidator resourceValidator);
}
