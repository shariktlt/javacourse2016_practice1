package ru.innopolis.sharypov.practice1.resource.processors;

import ru.innopolis.sharypov.practice1.resource.validators.ResourceValidator;

import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Created by innopolis on 13.10.16.
 */
public class CountWordsProcessor extends BasicProcessor {
    //private Scanner scanner;
    //private StringTokenizer stringTokenizer;
    private int processed;
    private String[] tokens;
    @Override
    protected void prepare() {
        super.prepare();
        logger.info("Create scanner for {}", getResourceObject().getRaw());
        tokens = getResourceObject().getText().split("[\\s\\r\\n]+");
        //scanner = new Scanner(resourceObject.getText()).useDelimiter( "[\\s\\r\\n]+" );
        //stringTokenizer = new StringTokenizer(resourceObject.getText());
    }

    @Override
    protected void cleanup() {
        super.cleanup();
        logger.info("Close scanner");
        //scanner.close();
       // stringTokenizer.
    }

    @Override
    protected boolean hasNextIteration() {
        return processed < tokens.length;
    }

    @Override
    protected void iterate() {
        boolean hasNext;
        try {
            String token = tokens[ processed++ ];
            if(getResourceValidator().isValid(token)) {
                String filtered = filterToken(token);
                logger.debug("Next token {}:{}[{}]", token, filtered, filtered.length());
                if (filtered.length() > 0) {
                    getRegistryHandler().register(filtered);
                }
                //sleep(20);
            }else{
                logger.error("Found non cyrillic symbol in token {} at {}", token, getResourceObject().getRaw());
                getRegistryHandler().setProcessing(false);
            }

        } catch (Exception e) {
            logger.error("Iterate got exception", e);
        }

    }

    private String filterToken(String token){
        return token.replaceAll("[^а-яА-ЯЁё]+", "").toLowerCase();
    }

    @Override
    public Object getNewObject() {
        return new CountWordsProcessor();
    }


}
