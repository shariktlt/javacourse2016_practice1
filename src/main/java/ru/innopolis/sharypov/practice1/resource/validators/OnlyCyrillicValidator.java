package ru.innopolis.sharypov.practice1.resource.validators;

import ru.innopolis.sharypov.practice1.resource.FactoryInterface;

import java.util.regex.Pattern;

/**
 * Created by innopolis on 13.10.16.
 */
public class OnlyCyrillicValidator implements ResourceValidator {
    @Override
    public boolean isValid(String string) {
        return string.matches( "^[^A-Za-z\\s]*$" );
    }

    @Override
    public Object getNewObject() {
        return new OnlyCyrillicValidator();
    }
}
