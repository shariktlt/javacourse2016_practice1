package ru.innopolis.sharypov.practice1.resource.loaders;

import ru.innopolis.sharypov.practice1.resource.FactoryInterface;
import ru.innopolis.sharypov.practice1.resource.ResourceObject;

/**
 * Created by innopolis on 11.10.16.
 */
public interface LoadableResource extends FactoryInterface {
    String loadString(ResourceObject resourceObject);
}
