package ru.innopolis.sharypov.practice1.resource.loaders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.sharypov.practice1.resource.ResourceObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by innopolis on 11.10.16.
 */
public class FileResource implements LoadableResource {
    private Logger logger = LoggerFactory.getLogger( FileResource.class );
    @Override
    public String loadString(ResourceObject resourceObject) {
        return readFile(resourceObject.getPath(), StandardCharsets.UTF_8);
    }

    private String readFile(String path, Charset encoding){
        byte[] encoded;
        try {
            encoded = Files.readAllBytes( Paths.get(path));
        } catch (IOException e) {
            logger.error( e.getMessage() );
            return null;
        }
        return new String(encoded, encoding);
    }

    @Override
    public Object getNewObject() {
        return new FileResource();
    }
}
