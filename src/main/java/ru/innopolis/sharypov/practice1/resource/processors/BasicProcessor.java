package ru.innopolis.sharypov.practice1.resource.processors;

import org.apache.log4j.lf5.util.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.sharypov.practice1.RegistryHandler;
import ru.innopolis.sharypov.practice1.resource.FactoryInterface;
import ru.innopolis.sharypov.practice1.resource.ResourceObject;
import ru.innopolis.sharypov.practice1.resource.validators.ResourceValidator;

/**
 * Created by innopolis on 13.10.16.
 */
public abstract class   BasicProcessor implements ResourceProcessor{
    private ResourceObject resourceObject;
    private RegistryHandler registryHandler;
    private ResourceValidator resourceValidator;
    protected Logger logger = LoggerFactory.getLogger( this.getClass() );

    protected void prepare(){}
    protected void iterate(){}
    protected void cleanup(){}

    @Override
    public void processResource(ResourceObject resourceObject, RegistryHandler registryHandler, ResourceValidator resourceValidator) {
        this.resourceObject = resourceObject;
        this.registryHandler = registryHandler;
        this.resourceValidator = resourceValidator;
        try {
            prepare();
            loop();
        }catch (Exception e){
            logger.error("Got exception!", e);
        }finally {
            cleanup();
        }
    }

    protected void loop(){
        while(!Thread.currentThread().isInterrupted() && registryHandler.isProcessing() && hasNextIteration()){
            iterate();
        }
    }

    protected void sleep(long msec){
        try {
            Thread.sleep( msec );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected boolean hasNextIteration(){
        return false;
    }

    public ResourceObject getResourceObject() {
        return resourceObject;
    }

    public ResourceValidator getResourceValidator() {
        return resourceValidator;
    }

    public RegistryHandler getRegistryHandler() {
        return registryHandler;
    }

    public void setResourceValidator(ResourceValidator resourceValidator){
        this.resourceValidator = resourceValidator;
    }
}
