package ru.innopolis.sharypov.practice1.resource;

/**
 * Created by innopolis on 12.10.16.
 */
public class ResourceObject {
    private final boolean valid;
    private final String raw;
    private final String protocol;
    private final String path;
    private String text;

    public ResourceObject(String raw) {
        String[] str = raw.split("\\:\\/\\/");
        boolean valid = str.length == 2;
        this.raw = raw;
        if(valid){
            this.protocol = str[0];
            this.path = str[1];
            valid = (this.protocol.length() > 0);
        }else{
            this.protocol = "";
            this.path = "";
        }
        this.valid = valid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isValid() {
        return valid;
    }

    public String getRaw() {
        return raw;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getPath() {
        return path;
    }
}
