package ru.innopolis.sharypov.practice1;

/**
 * Created by innopolis on 12.10.16.
 */
public class ResultEntry {
    private int count;

    public synchronized void  incriment(){
        count++;
    }

    public synchronized int value() {
        return count;
    }
}
