package ru.innopolis.sharypov.practice1.threads;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.slf4j.MDC;
import ru.innopolis.sharypov.practice1.RegistryHandler;
import ru.innopolis.sharypov.practice1.resource.ResourceObject;
import ru.innopolis.sharypov.practice1.resource.loaders.LoadableResource;
import ru.innopolis.sharypov.practice1.resource.processors.ResourceProcessor;
import ru.innopolis.sharypov.practice1.resource.validators.ResourceValidator;

/**
 * Created by innopolis on 12.10.16.
 */
public class ProcessorThread extends BasicThread implements Runnable {
    private ResourceObject resourceObject;
    private RegistryHandler registryHandler;
    private LoadableResource loadableResource;
    private ResourceValidator resourceValidator;
    private ResourceProcessor resourceProcessor;
    private Logger logger = LoggerFactory.getLogger( ProcessorThread.class );

    public ProcessorThread(ResourceObject resourceObject, RegistryHandler registryHandler, LoadableResource loadableResource, ResourceValidator resourceValidator, ResourceProcessor resourceProcessor) {
        this.resourceObject = resourceObject;
        this.registryHandler = registryHandler;
        this.loadableResource = loadableResource;
        this.resourceValidator = resourceValidator;
        this.resourceProcessor = resourceProcessor;
    }

    @Override
    public void run() {
        logger.debug("Start load");
        String text = loadableResource.loadString(resourceObject);
        if(text == null){
            logger.warn("Null text in {}", resourceObject.getRaw());
            return;
        }
        logger.debug("Text loaded from \n{}", resourceObject.getRaw());
        resourceObject.setText( text );
        MDC.put("file",resourceObject.getRaw());
        if(resourceProcessor == null){
            logger.error("Processor was not setted");
            return;
        }
        resourceProcessor.processResource( resourceObject, registryHandler, resourceValidator );
    }
}
