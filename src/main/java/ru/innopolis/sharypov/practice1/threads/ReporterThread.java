package ru.innopolis.sharypov.practice1.threads;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.sharypov.practice1.RegistryHandler;
import ru.innopolis.sharypov.practice1.reporters.ReporterInterface;

/**
 * Created by innopolis on 12.10.16.
 */
public class ReporterThread extends BasicThread implements Runnable {
    private RegistryHandler registryHandler;
    private Logger logger = LoggerFactory.getLogger(ReporterThread.class);
    private ReporterInterface reporterInterface;
    private Long interval;

    public ReporterThread(RegistryHandler registryHandler, ReporterInterface reporterInterface, Long interval) {
        this.registryHandler = registryHandler;
        this.interval = interval;
        this.reporterInterface = reporterInterface;
    }


    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted() && registryHandler.isProcessing()){
            //sleep(interval);
            synchronized (registryHandler){
                try {
                    registryHandler.wait(interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            logger.info("Generate report");
            reporterInterface.reportRegistry( registryHandler );
        }
    }
}
