package ru.innopolis.sharypov.practice1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.sharypov.practice1.reporters.FileReporter;
import ru.innopolis.sharypov.practice1.reporters.ReporterInterface;
import ru.innopolis.sharypov.practice1.resource.loaders.FileResource;
import ru.innopolis.sharypov.practice1.resource.loaders.HttpResource;
import ru.innopolis.sharypov.practice1.resource.loaders.LoadableResource;
import ru.innopolis.sharypov.practice1.resource.ResourceObject;
import ru.innopolis.sharypov.practice1.resource.processors.CountWordsProcessor;
import ru.innopolis.sharypov.practice1.resource.processors.ResourceProcessor;
import ru.innopolis.sharypov.practice1.resource.validators.OnlyCyrillicValidator;
import ru.innopolis.sharypov.practice1.resource.validators.ResourceValidator;
import ru.innopolis.sharypov.practice1.threads.ProcessorThread;
import ru.innopolis.sharypov.practice1.threads.ReporterThread;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.lang.Thread.sleep;

/**
 * Created by innopolis on 11.10.16.
 *
 * Необходимо разработать программу, которая получает на вход список ресурсов, содержащих текст, и считает общее количество
 * вхождений (для всех ресурсов) каждого слова. Каждый ресурс должен быть обработан в отдельном потоке, текст не должен
 * содержать инностранных символов, только кириллица, знаки препинания и цифры. Отчет строится в режиме реального времени,
 * знаки препинания и цифры в отчет не входя. Все ошибки должны корректно обработаны, все API покрыто модульными тестами.
 *
 * Pass resource url (http://URL, https://URL, file://PATH) separated by space as argument of program.
 * For test purposes directory /test_texts/ contain large text files
 * All texts should be only in UTF-8 charset
 * Example of arguments line:
 *  file://./test_texts/grad.txt file://./test_texts/hotel.txt file://./test_texts/text1.txt file://./test_texts/text2.txt file://./test_texts/unarmed.txt file://./test_texts/wavewind.txt http://events.shariktlt.ru/test_text1.txt
 *
 */
public class Main {
    private Map<String, LoadableResource> loadableResourceMap;
    private ResourceProcessor resourceProcessor = new CountWordsProcessor();
    private ResourceValidator resourceValidator = new OnlyCyrillicValidator();
    private RegistryHandler registryHandler = new RegistryHandler();
    private ReporterInterface reporterInterface = new FileReporter("./report.txt");
    private Set<Thread> threads = new HashSet<>();
    private Thread reporterThread;

    public static Logger logger = LoggerFactory.getLogger( Main.class );

    public Main() {
        loadableResourceMap = new HashMap<>();
        loadableResourceMap.put( "https", new HttpResource() );
        loadableResourceMap.put( "http", new HttpResource() );
        loadableResourceMap.put( "file", new FileResource() );
    }

    public static void main(String[] args) {
        try {
            Main main = new Main();
            main.start( args );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start(String[] args) {
        logger.info( "Start with args " + String.join( ", ", args ) );
        for (String resText : args) {
            logger.info( "Check: {}", resText );
            ResourceObject resourceObject = new ResourceObject( resText );
            if (resourceObject.isValid()) {
                LoadableResource loadableResource = loadableResourceMap.get( resourceObject.getProtocol() );
                if (loadableResource != null) {
                    logger.debug( "Create processor for {}", resourceObject.getRaw() );
                    ResourceProcessor workerProcessor = (ResourceProcessor) resourceProcessor.getNewObject();
                    ResourceValidator resourceValidator = (ResourceValidator) this.resourceValidator.getNewObject();
                    workerProcessor.setResourceValidator(resourceValidator);
                    Thread thread = new Thread(new ProcessorThread(resourceObject, registryHandler, loadableResource, resourceValidator, workerProcessor ));
                    threads.add(thread);
                    thread.start();
                }
            }
        }
        if (threads.size() > 0) {
            reporterThread = new Thread(new ReporterThread( registryHandler, reporterInterface, new Long(5000) ));
            reporterThread.start();
            waitComplete();

        } else {
            logger.warn( "Nothing to process" );
        }

    }

    private void waitComplete() {
        logger.debug( "Start waiting" );
        boolean isWaiting = true;
        Thread.currentThread().setPriority( 1 );
        while (isWaiting && !Thread.currentThread().isInterrupted()) {
            isWaiting = false;
            for (Thread t : threads) {
                if (t.isAlive()) {
                    if (!registryHandler.isProcessing()) {
                        logger.info( "Interrupt thread {}", t.getName() );
                        t.interrupt();
                    } else {
                        isWaiting = true;
                    }
                }
            }
            try {
                if (isWaiting) {
                    sleep( 1000 );
                }else{
                    registryHandler.setProcessing(false);
                    synchronized (registryHandler){
                        registryHandler.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }

        }
    }
}
