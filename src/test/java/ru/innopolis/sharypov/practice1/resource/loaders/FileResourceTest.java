package ru.innopolis.sharypov.practice1.resource.loaders;

import org.junit.Before;
import org.junit.Test;
import ru.innopolis.sharypov.practice1.resource.ResourceObject;

import static org.junit.Assert.*;

/**
 * Created by innopolis on 13.10.16.
 */
public class FileResourceTest {
    LoadableResource loadableResource;

    @Before
    public void before(){
        loadableResource = new FileResource();
    }
    @Test
    public void loadString() throws Exception {
        assertNull( "Load not exist file", loadableResource.loadString( new ResourceObject( "file://./not_exist_file" ) ) );
        assertNotNull("Load exist file", loadableResource.loadString( new ResourceObject( "file://./test_texts/grad.txt" ) ));
    }
    @Test
    public void testNewObject(){
        LoadableResource r1 = (LoadableResource) loadableResource.getNewObject();
        LoadableResource r2 = (LoadableResource) loadableResource.getNewObject();
        assertNotNull(r1);
        assertNotNull(r2);
        assertFalse(r1 == r2);
        assertFalse(r1.equals(r2));
    }

}