package ru.innopolis.sharypov.practice1.resource.validators;

import org.junit.BeforeClass;
import org.junit.Test;
import ru.innopolis.sharypov.practice1.resource.loaders.LoadableResource;

import static org.junit.Assert.*;

/**
 * Created by shariktlt on 13.10.16.
 */
public class OnlyCyrillicValidatorTest {
    static ResourceValidator onlyCyrillicValidator;
    @BeforeClass
    public static void setUp() throws Exception {
        onlyCyrillicValidator = new OnlyCyrillicValidator();
    }

    @Test
    public void validate() throws Exception {
        assertFalse("Check english", onlyCyrillicValidator.isValid("abc"));
        assertFalse("Check cyrilliс word with english letter", onlyCyrillicValidator.isValid("Данtе"));
        assertTrue("Check cyrilliс word", onlyCyrillicValidator.isValid("Данте"));
        assertTrue("Minus", onlyCyrillicValidator.isValid("Как-то"));
        assertTrue("Comma", onlyCyrillicValidator.isValid("запятая,"));
        assertTrue("Dot", onlyCyrillicValidator.isValid("точка."));
        assertFalse("Delimiter", onlyCyrillicValidator.isValid("тест пробела"));
        assertTrue("No words", onlyCyrillicValidator.isValid("..."));
    }

    @Test
    public void testNewObject(){
        ResourceValidator r1 = (ResourceValidator) onlyCyrillicValidator.getNewObject();
        ResourceValidator r2 = (ResourceValidator) onlyCyrillicValidator.getNewObject();
        assertNotNull(r1);
        assertNotNull(r2);
        assertFalse(r1 == r2);
        assertFalse(r1.equals(r2));
    }

}