package ru.innopolis.sharypov.practice1.resource.loaders;


import org.junit.Before;
import org.junit.Test;
import ru.innopolis.sharypov.practice1.resource.ResourceObject;

import static org.junit.Assert.*;

/**
 * Created by shariktlt on 13.10.16.
 */
public class HttpResourceTest {
    LoadableResource loadableResource;
    @Before
    public void setUp() throws Exception {
        loadableResource = new HttpResource();
    }

    @Test
    public void loadString() throws Exception {
        assertNotNull("Load exist file over http", loadableResource.loadString(new ResourceObject("http://events.shariktlt.ru/test_text1.txt")));
        assertNotNull("Load exist file over https", loadableResource.loadString(new ResourceObject("https://events.shariktlt.ru:4443/test_text1.txt")));
        assertNull("Load not exist file over http", loadableResource.loadString(new ResourceObject("http://events.shariktlt.ru/NOT_EXIST.txt")));
        assertNull("Load not exist file over https", loadableResource.loadString(new ResourceObject("https://events.shariktlt.ru:4443/NOT_EXIST.txt")));

    }

    @Test
    public void testNewObject(){
        LoadableResource r1 = (LoadableResource) loadableResource.getNewObject();
        LoadableResource r2 = (LoadableResource) loadableResource.getNewObject();
        assertNotNull(r1);
        assertNotNull(r2);
        assertFalse(r1 == r2);
        assertFalse(r1.equals(r2));
    }


}