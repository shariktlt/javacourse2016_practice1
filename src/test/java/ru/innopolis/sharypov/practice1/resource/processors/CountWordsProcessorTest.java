package ru.innopolis.sharypov.practice1.resource.processors;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.innopolis.sharypov.practice1.resource.validators.ResourceValidator;

import static org.junit.Assert.*;

/**
 * Created by shariktlt on 15.10.16.
 */
public class CountWordsProcessorTest {
    ResourceProcessor resourceProcessor;
    @Before
    public void setUp(){
        resourceProcessor = new CountWordsProcessor();
    }
    @Test
    public void getNewObject() throws Exception {
        ResourceProcessor r1 = (ResourceProcessor) resourceProcessor.getNewObject();
        ResourceProcessor r2 = (ResourceProcessor) resourceProcessor.getNewObject();
        assertNotNull(r1);
        assertNotNull(r2);
        assertFalse(r1 == r2);
        assertFalse(r1.equals(r2));

    }

}