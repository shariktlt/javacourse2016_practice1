package ru.innopolis.sharypov.practice1.resource;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by innopolis on 13.10.16.
 */
public class ResourceObjectTest {


    @Test
    public void isValid() throws Exception {
        assertTrue("Valid file link", new ResourceObject( "file:///some_url").isValid() );
        assertTrue("Valid http link", new ResourceObject( "http:///some_url").isValid() );
        assertTrue("Valid https link", new ResourceObject( "https:///some_url").isValid() );
        assertFalse("Invalid link, no protocol", new ResourceObject( "/some/url" ).isValid());
        assertFalse("Invalid link, double protocol", new ResourceObject( "file://file://some_url/" ).isValid());
        assertFalse("Empty protocol", new ResourceObject( "://some_url/" ).isValid());
        ResourceObject resourceObject = new ResourceObject("file:///some/path");
        assertEquals("file", resourceObject.getProtocol());
        assertEquals("/some/path", resourceObject.getPath());
        resourceObject.setText("TextOfObject");
        assertEquals("TextOfObject", resourceObject.getText());
    }

}