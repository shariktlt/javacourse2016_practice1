package ru.innopolis.sharypov.practice1;

import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by innopolis on 13.10.16.
 */
public class RegistryHandlerTest {
    private RegistryHandler registryHandler;
    @Before
    public void setUp() throws Exception {
        registryHandler = new RegistryHandler();
    }

    @Test
    public void register() throws Exception {
        assertEquals("Initial zero map size", (Integer) 0, (Integer) registryHandler.getResults().size());
        registryHandler.register("TestString");
        assertEquals("Check count of registered elements", (Integer) 1, (Integer) registryHandler.getResults().size());
        assertEquals("Check if value of TestString equals 1", (Integer) 1, (Integer) ((Map.Entry<String, ResultEntry>) registryHandler.getResults().toArray()[0]).getValue().value());

        registryHandler.register("TestString");
        assertEquals("Check count of registered elements", (Integer) 1, (Integer) registryHandler.getResults().size());
        assertEquals("Check if value of TestString equals 2", (Integer) 2, (Integer) ((Map.Entry<String, ResultEntry>) registryHandler.getResults().toArray()[0]).getValue().value());

        registryHandler.register("TestString2");
        assertEquals("Check count of registered elements", (Integer) 2, (Integer) registryHandler.getResults().size());

        Iterator iterator  = registryHandler.getResults().iterator();
        while(iterator.hasNext()){
            Map.Entry<String, ResultEntry> item = (Map.Entry<String, ResultEntry>) iterator.next();
            if( "TestString2".equals(item.getKey()) ){
                assertEquals("Check if value of TestString2 equals 1", (Integer) 1, (Integer) item.getValue().value());
            }else{
                assertEquals("Check if value of TestString1 equals 2", (Integer) 2, (Integer) item.getValue().value());
            }
        }


    }

}