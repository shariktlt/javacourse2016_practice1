package ru.innopolis.sharypov.practice1.reporters;

import org.junit.Before;
import org.junit.Test;
import ru.innopolis.sharypov.practice1.RegistryHandler;
import ru.innopolis.sharypov.practice1.resource.ResourceObject;
import ru.innopolis.sharypov.practice1.resource.loaders.FileResource;

import static org.junit.Assert.*;

/**
 * Created by innopolis on 14.10.16.
 */
public class FileReporterTest {
    RegistryHandler registryHandler;
    FileReporter fileReporter;
    ResourceObject resourceObject;
    @Before
    public void setUp() throws Exception {
        resourceObject = new ResourceObject("file://./build/test.txt");
        registryHandler = new RegistryHandler();
        registryHandler.register("test");
        fileReporter = new FileReporter(resourceObject.getPath());
    }

    @Test
    public void reportRegistry() throws Exception {
        fileReporter.reportRegistry(registryHandler);
        String result = (new FileResource()).loadString(resourceObject);
        assertEquals("test:1\n", result);
    }

}