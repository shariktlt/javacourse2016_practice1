package ru.innopolis.sharypov.practice1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by shariktlt on 14.10.16.
 */
public class ResultEntryTest {
    ResultEntry resultEntry;
    @Before
    public void setUp() throws Exception {
        resultEntry = new ResultEntry();
    }

    @Test
    public void testValues() throws Exception {
        assertEquals("Check initial zero", (long) 0, (long) resultEntry.value());
        resultEntry.incriment();
        assertEquals("Check 1", (long) 1, (long) resultEntry.value());
    }



}